"""Generates the ALTiCU sites"""

import logging

from json import loads
from os import path, makedirs
from shutil import copytree, rmtree
from urllib.request import urlopen
from collections import namedtuple
from glob import iglob
from re import compile
from sys import stdout
from random import randint

from jinja2 import Environment, FileSystemLoader
from htmlmin import Minifier

EMAIL_REGEX = compile(r"[\w\d]+@[\w\d]+\.[\w\d]{2,}")

logging.basicConfig(
    format="[%(asctime)s][%(levelname)s]: %(message)s",
    datefmt="%c", stream=stdout, level=logging.DEBUG)

# Functions

def obfuscate_string(string):
    obfuscated = ""
    for c in string:
        if randint(0, 10) > 6:
            obfuscated += c
        else:
            obfuscated += "&#" + str(ord(c)) + ";"
    return obfuscated

def is_online(url):
    """Gets the HTTP status code for a URL"""
    logging.info("Checking if %s is online...", url)
    c = 500
    try:
        conn = urlopen(url, timeout=1)
        c = conn.getcode()
        conn.close()
    except:
        pass
    return c >= 200 and c < 400


def d_to_nt(obj):
    """Convert a dict to a namedtuple"""
    return namedtuple('Struct', obj.keys())(*obj.values())

# Parsing

logging.info("Generating alt.icu site...")

DATA_S = {}

logging.info("Reading data files...")
for data_source in ["projects", "services"]:
    with open("data/%s.json" % data_source) as f:
        raw_json = loads(f.read())
        raw_json.sort(key=lambda x: x["name"])
        if data_source == "services":
            for service in raw_json:
                if service["type"] != "Coming Soon":
                    service["online"] = is_online(service["url"])
                    if not service["online"]:
                        service["type"] = "Offline"

                    service["weight"] = 1
                    service["weight"] += service["online"]
                    if service["type"] == "Public":
                        service["weight"] += 0.5
                else:
                    service["online"] = False
                    service["weight"] = 0
        if "weight" in raw_json[0]:
            raw_json.sort(key=lambda x: x["weight"], reverse=True)
        DATA_S[data_source] = [d_to_nt(i) for i in raw_json]

with open("data/meta.json") as f:
    DATA_S["_"] = d_to_nt(loads(f.read()))

ENV = Environment(loader=FileSystemLoader("templates"), trim_blocks=True)

PATHS = ["public", "public/portal", "public/about"]

for p in PATHS:
    if not path.exists(p):
        makedirs(p)

if path.exists("public/assets"):
    rmtree("public/assets")
copytree("static", "public/assets")

MINIFIER = Minifier(
                    remove_comments=True,
                    remove_all_empty_space=True,
                    reduce_boolean_attributes=True,
                    convert_charrefs=True)

for template in iglob("templates/**/*.html", recursive=True):
    if "base" in template:
        continue

    DATA_S["file"] = template
    template = template[10:]

    out_file = path.join("public", template)

    logging.info("Rendering %s...", out_file)

    if not path.exists(path.dirname(out_file)):
        makedirs(path.dirname(out_file))

    t = ENV.get_template(template)
    h = t.render(title="ALTiCU", **DATA_S)

    h_min = MINIFIER.minify(h)

    emails = EMAIL_REGEX.findall(h_min)
    for email in emails:
        h_min = h_min.replace(email, obfuscate_string(email))

    with open(out_file, "w+") as f:
        f.write(h_min)

logging.info("Done generating alt.icu!")
