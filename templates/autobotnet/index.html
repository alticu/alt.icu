<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" href="/assets/css/m-dark.min.css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="theme-color" content="#22272e" />
  <meta name="og:site_name" content="ALTiCU" />
  <meta charset="utf-8">
  <link rel="icon" href="/assets/img/autobotnet.png">

  <title>AutoBotnet</title>
</head>

<body>
  <header>
    <nav id="navigation">
      <div class="m-container">
        <div class="m-row">
          <a href="#" id="m-navbar-brand" class="m-col-t-8 m-col-m-none m-left-m">AutoBotnet</a>
          <a id="m-navbar-show" href="#navigation" title="Show navigation" class="m-col-t-3 m-hide-m m-text-right"></a>
          <a id="m-navbar-hide" href="#" title="Hide navigation" class="m-col-t-3 m-hide-m m-text-right"></a>
          <div id="m-navbar-collapse" class="m-col-t-12 m-show-m m-col-m-none m-right-m">
            <div class="m-row">
              <ol class="m-col-t-6 m-col-m-none">
                <li>
                  <a href="https://alt.icu">ALTiCU</a>
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </header>

  <main>
    <div class="m-container m-container-inflatable">
      <div class="m-row">
        <article class="m-col-l-10 m-push-l-1">
          <section id="intro">
            <img class="m-image" src="/assets/img/autobotnet.png" alt="autobotnet logo" />
            <h1><a id="AutoBotnet_1"></a>AutoBotnet</h1>
            <p>AutoBotnet is an open source game <strong>centered on strategic programming</strong>, which controls a
              more broad game based on resource management and empire building.</p>
            <p>The game takes place as a massively multiplayer competition between human players, who control a
              hivemind (“botnet”) of robotic agents to subjugate all other empires and dominate the World.</p>
            <h2><a id="Programming_Centric_7"></a>Programming Centric</h2>
            <p>AutoBotnet is not a typical strategy video game. There are no carefully animated characters or fancy
              graphics. There is no point-and-click user interface for commanding units.</p>
            <p>Instead, we believe that ultimate freedom for the player comes from being able to control every aspect
              of an entire empire’s behavior by writing scripts.</p>
            <p>AutoBotnet then becomes a competition not of reflexes or even just of strategic thinking, but is also a
              competition of articulating complex strategies as efficient and effective code.</p>
            <p>Some real examples:</p>
            <pre><code class="language-js"><span class="hljs-comment">// use spare resources to build bots</span>
    <span class="hljs-keyword">var</span> resources = army.resources();
    <span class="hljs-keyword">if</span> (resources[<span class="hljs-string">"nrg"</span>] &gt; Config.RESERVE_NRG) {
        <span class="hljs-keyword">var</span> bot = army.construct(<span class="hljs-string">"scout"</span>);
        <span class="hljs-comment">// make a storage and mining cores</span>
        <span class="hljs-keyword">var</span> storageCore = bot.installCore(<span class="hljs-string">"CoreStorage1"</span>);
        <span class="hljs-keyword">var</span> drillCore = bot.installCore(<span class="hljs-string">"CoreDrill1"</span>);
        <span class="hljs-comment">// -</span>
        <span class="hljs-comment">// move the bot north for an unspecified reason</span>
        bot.move(C.NORTH);
    }
    </code></pre>
            <h3><a id="Programming_Concept_Componentbased_architecture_31"></a>Programming Concept: Component-based
              architecture</h3>
            <p>A recurring element throughout AutoBotnet is that of component-based architecture. Because nearly
              everything in the game is made up in some way of smaller components that can be combined in specific ways
              for different purposes, there is a great degree in freedom in how certain goals can be acheived.</p>
            <p>For example, a mining bot might have two <code>DRILL</code> cores and four <code>STORAGE</code> cores,
              while a scout bot might have a <code>CLOAK</code> core and the very large <code>SPEED</code> exoskeleton.</p>
            <p>Bots are then “containers” (which come in different capacities and durabilities) for combinations of a
              large selection of cores. A bot “has” cores, which define its functionality.</p>
            <h2><a id="A_Botnet_of_Agents_39"></a>A Botnet of Agents</h2>
            <p>Each player becomes the commander of their own Empire, which is a hivemind botnet where all bots are one
              for the purpose of the Empire.</p>
            <h2><a id="Infinite_Exhaustible_World_43"></a>Infinite, Exhaustible World</h2>
            <p>The world in AutoBotnet is an infinite grid of <strong>Rooms</strong>, which are large, interconnected
              square-shaped areas. Rooms are procedurally generated by a complex process and every empire begins
              isolated in its own room.</p>
            <p>Rooms will contain a variety of raw resources available for extraction and exploitation. However, nearly
              all raw resource deposits have a very limited quantity and a botnet must colonize other rooms and defend
              them from other expansionist empires.</p>
            <h2><a id="Emergent_Mechanics_Diplomacy_and_Economy_49"></a>Emergent Mechanics, Diplomacy, and Economy</h2>
            <p>Because the game consists of intelligent human players scripting their empire, there is much to be
              gained from allowing inter-empire interaction through the game.</p>
            <p>Using <strong>Communication Beacons</strong>, empires can transmit and receive data from other empires,
              which can be used to coordinate large military operations or economic expansion.</p>
            <h2><a id="Metagaming_55"></a>Metagaming</h2>
            <p>A botnet needs something to store information and coordinate its operation. The <strong>Datacenter</strong>
              building takes this role, providing a large amount of data storage for storing information on targets,
              observations on other empires, and keeping track of units.</p>
            <p>In AutoBotnet, datacenters can be prime targets because they can be shut down or hacked with physical
              access. An empire can destroy its rival’s datacenters to destroy information and just generally wreak
              havoc.</p>
            <h2><a id="Encouraging_Efficient_Code_61"></a>Encouraging Efficient Code</h2>
            <p>The AutoBotnet game runs by allowing each empire to “tick” according to the tickrate, which is usually
              anywhere around 1-5 seconds. Every tick, each empire’s logic is ticked and the game steps.</p>
            <h3><a id="Limits_and_Sandboxing_65"></a>Limits and Sandboxing</h3>
            <p>Because the game will be running arbitrary player provided scripts, the player script is run in a code
              scripting engine sandbox with various limits, including a run-time limit, a recursion limit, and a heap
              memory limit.</p>
            <p>Because the script must load and run within the short fraction of a tick allotted to a particular
              empire, efficient scripts can do more and make full use of their empire’s abilities.</p>
            <h2><a id="Perpetual_Gameplay_71"></a>Perpetual Gameplay</h2>
            <p>The game never stops. Even when a player logs off, their empire continues to follow its instructions and
              interact with the world and other empires.</p>
            <h2><a id="Extensibility_from_the_Ground_Up_75"></a>Extensibility from the Ground Up</h2>
            <p>The AutoBotnet server is <em>extremely</em> extensible. The server features dynamically discovering and
              loading .NET plugins that can replace or extend almost any component of the game.</p>
            <p>To demonstrate this, the entirety of the base game is implemented as a group of default plugins,
              including <code>DefaultEconomyPlugin</code>, <code>DefaultBotTemplatesPlugin</code>, <code>CoreMapPlugin</code>,
              and <code>JSScriptEnginePlugin</code>, among others.</p>
            <p>Even the web services module is implemented to allow extensibility, so by referencing the <code>Speercs.Server</code>
              assembly, the game can be deeply modified without ever needing to alter the game’s binaries.</p>
          </section>
        </article>
        <article class="m-col-l-10 m-push-l-1">
          <section id="wip">
            <img class="m-image" src="/assets/img/autobotnet.png" alt="autobotnet logo" />
            <h1>WIP</h1>
            <p>
              AutoBotnet is an upcoming scripting-based RTS game, Coming Soon&trade;. Brought to you by The
              CookieEaters.
            </p>
            <p>
              Keep an eye on our blog for updates.
            </p>
            <div class="m-button m-primary">
              <a href="https://blog.alt.icu">Blog</a>
            </div>
          </section>
          <section id="info">
            <h2>Powered by ALTiCU</h2>
            <p>
              AutoBotnet development is supported and driven by ALTiCU.
            </p>
            <img class="m-image" src="/assets/img/alticu.png" alt="alticu logo" />
          </section>
        </article>
      </div>
    </div>
  </main>

  <footer>
    <nav>
      <div class="m-container">
        <div class="m-row">
          <div class="m-col-l-10 m-push-l-1">
            <p>
              Copyright &copy; 2018 The CookieEaters. Supported by
              <a href="https://alt.icu">ALTiCU</a>.
            </p>
            <p>
              <a href="#">This work</a> is licensed under a
              <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons
                Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.
            </p>
          </div>
        </div>
      </div>
    </nav>
  </footer>
</body>

</html>
